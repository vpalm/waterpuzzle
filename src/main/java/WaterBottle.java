public class WaterBottle {
    private int capacity;
    private int waterAmount;

    public WaterBottle(int capacity, int waterAmount) {
        this.capacity = capacity;
        this.waterAmount = waterAmount;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(int waterAmount) {
        this.waterAmount = waterAmount;
    }

    public void fillBottle() {
        waterAmount = capacity;
    }

    public void emptyBottle() {
        waterAmount = 0;
    }

}
