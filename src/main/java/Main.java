public class Main {
    private static int counter = 0;

    public static void main(String args[]) {
        WaterBottle firstBottle = new WaterBottle(3, 0);
        WaterBottle secondBottle = new WaterBottle(5, 0);

        boolean firstBottleOneLiter = false;
        boolean secondBottleOneLiter = false;
        boolean secondBottleFourLiters = false;

        int stepsToFirst = 0;
        int stepsToSecond = 0;
        int stepsToThird = 0;

        do {
            //if bottle is empty, fill it
            if(firstBottle.getWaterAmount() == 0) {
                fillBottle(firstBottle);
            }

            pourWater(firstBottle, secondBottle);

            //check result
            if (firstBottle.getWaterAmount() == 1) {
                firstBottleOneLiter = true;
                stepsToFirst = counter;

                //reset bottles and counter before we change direction
                counter  = 0;
                emptyBottle(firstBottle);
                emptyBottle(secondBottle);
            }
        } while(!firstBottleOneLiter);

        //change the pouring direction
        do {
            //if bottle is empty, fill it
            if (secondBottle.getWaterAmount() == 0) {
                fillBottle(secondBottle);
            }

            pourWater(secondBottle, firstBottle);

            //check result
            if (secondBottle.getWaterAmount() == 1) {
                secondBottleOneLiter = true;
                stepsToSecond = counter;
            }

            if (secondBottle.getWaterAmount() == 4) {
                secondBottleFourLiters = true;
                stepsToThird = counter;
            }

        } while(!secondBottleOneLiter || !secondBottleFourLiters);

        System.out.println("Counter " + counter);
        System.out.println("Steps to first " + stepsToFirst);
        System.out.println("Steps to second " + stepsToSecond);
        System.out.println("Steps to third " + stepsToThird);
    }

    public static void pourWater(WaterBottle from, WaterBottle to) {
        counter++;

        //everything doesn't fit, a bit left in the first bottle
        if (to.getWaterAmount() + from.getWaterAmount() > to.getCapacity()) {
            from.setWaterAmount((to.getWaterAmount() + from.getWaterAmount()) - to.getCapacity());

            //empty the second bottle because here it became full
            emptyBottle(to);
        } else {
            //it's all good, everything fit into the second bottle
            to.setWaterAmount(to.getWaterAmount() + from.getWaterAmount());
            emptyBottle(from);

            //if the second bottle became full here, then empty it
            if(to.getWaterAmount() == to.getCapacity()) {
                emptyBottle(to);
            }
        }
    }

    public static void emptyBottle(WaterBottle bottle) {
        counter++;
        bottle.emptyBottle();
    }

    public static void fillBottle(WaterBottle bottle) {
        counter++;
        bottle.fillBottle();
    }
}
