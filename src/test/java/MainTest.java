import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest extends Main {

    private WaterBottle first;
    private WaterBottle second;

    @Test
    void fillFirstBottleTest() {
        first = new WaterBottle(3, 0);
        Main.fillBottle(first);
        assertEquals(3, first.getWaterAmount());
        assertNotEquals(5, first.getWaterAmount());
    }

    @Test
    void fillSecondBottleTest() {
        second = new WaterBottle(5, 0);
        Main.fillBottle(second);
        assertEquals(5, second.getWaterAmount());
        assertNotEquals(7, second.getWaterAmount());
    }

    @Test
    void emptyFirstBottleTest() {
        first = new WaterBottle(3, 2);
        Main.emptyBottle(first);
        assertEquals(0, first.getWaterAmount());
        assertNotEquals(5, first.getWaterAmount());
    }

    @Test
    void emptySecondBottleTest() {
        second = new WaterBottle(5, 5);
        Main.emptyBottle(second);
        assertEquals(0, second.getWaterAmount());
        assertNotEquals(5, second.getWaterAmount());
    }

    @Test
    void pourFromFirstToSecondBottle() {
        first = new WaterBottle(3, 3);
        second = new WaterBottle(5, 0);
        Main.pourWater(first, second);
        assertEquals(3, second.getWaterAmount());
        assertNotEquals(0, second.getWaterAmount());
    }

    @Test
    void pourFromFirstToSecondBottleWithOverflow() {
        first = new WaterBottle(3, 3);
        second = new WaterBottle(5, 3);
        Main.pourWater(first, second);
        //second is empty because i empty it direct after i realize that it's full
        assertEquals(0, second.getWaterAmount());
        assertEquals(1, first.getWaterAmount());
        assertNotEquals(5, second.getWaterAmount());
        assertNotEquals(2, first.getWaterAmount());
    }

    @Test
    void pourFromSecondToFirstBottle() {
        first = new WaterBottle(3, 0);
        second = new WaterBottle(5, 5);
        Main.pourWater(second, first);
        assertEquals(0, first.getWaterAmount());
        assertEquals(2, second.getWaterAmount());
        assertNotEquals(2, first.getWaterAmount());
        assertNotEquals(5, second.getWaterAmount());
    }
}